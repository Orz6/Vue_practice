// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import router from './router'

import store from './store'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import Vuex from 'vuex'

Vue.config.productionTip = false

Vue.use(ElementUI);

Vue.use(Vuex);

router.beforeEach((to,from,next)=>{
  // debugger
  let isLogin = sessionStorage.getItem("isLogin");

  // next()表示放行，安装原先的跳转逻辑放行
  // next({path:"/xxx"})，表示自定义跳转逻辑
  // 一般来说，二者选其一
  if (to.path=="/logout")
  {
    sessionStorage.clear();
    next({path:"/login"})
  }
  else if(to.path=="/login")
  {
    if(isLogin=="true")
    {
      next({path:"/main"})
    }
    else
    {
      next()
    }
  }
  else if (isLogin==null)
  {
    next({path:"/login"})
  }
  else
   {
     next();
   }


  console.log("into beforeEach,from.path="+from.path+",to.path="+to.path+",next.path="+next.path);



})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  router,
  store,
  render: h => h(App),
})
