const user = {
  state: sessionStorage.getItem("state") != "undefined" ? JSON.parse(sessionStorage.getItem("state")) : {
    user: {
      username: ''
    }
  },

  getters: {
    // state参数必须有
    getUser(state) {
      return state.user;
    }
  },

  mutations: {
    // state参数必须有
    updateUser(state, user) {
      state.user = user;
    }
  },

  actions: {
    asyncUpdateUser(context, user) {
      context.commit('updateUser', user);
    }
  }
}
export default user;
