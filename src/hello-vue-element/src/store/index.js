import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

// 非模块化的写法
// const state = sessionStorage.getItem("state") != "undefined" ? JSON.parse(sessionStorage.getItem("state")) : {
//   user: {
//     username: ''
//   }
// };
//
// const getters = {
//   // state参数必须有
//   getUser(state) {
//     return state.user;
//   }
// }
//
// const mutations = {
//   // state参数必须有
//   updateUser(state, user) {
//     state.user = user;
//   }
// }
//
// const actions = {
//   asyncUpdateUser(context, user) {
//     context.commit('updateUser', user);
//   }
// }
//
// // 构造函数Store是大写字母开头
// export default new Vuex.Store({
//   // vue组件里的用法为 this.$store.state
//   state,
//   getters,
//   mutations,
//   actions
// });


// 模块化的写法
import user from './modules/user'
export default new Vuex.Store({
  modules: {
    // vue组件里的用法为 // this.$store.state.user
    user,
  }
})
