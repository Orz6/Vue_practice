import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/Login'
import Main from '@/views/Main'

import UserList from '@/views/user/List'
import UserAdd from '@/views/user/Add'

Vue.use(Router);

export default new Router({
  routes: [
    {
      name: 'Login',
      path: '/login',
      component: Login,
    },
    {
      name: 'Main',
      path: '/main',
      component: Main,

      children: [
        {name: 'UserList', path: '/user/list', component: UserList},
        {name: 'UserAdd', path: '/user/add', component: UserAdd},
      ]
    }

  ]

})
