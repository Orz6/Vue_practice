var app = new Vue({

    el:'#app',
    data:{
        name:null,
        age:null,
        sex:null
    }
});


var app2 = new Vue({

    el:'#app2',
    data:{
        //foodList:['葱','姜','蒜'],

        foodList:[
            {
            "name":'葱',
            "price":10,
            "discount":.5,
            },
            {
                "name":'姜',
                "price":5,
                "discount":.8,
            },
            {
                "name":'蒜',
                "price":2,

            },

        ],
    }

});


var app4 = new Vue({
    el: '#app4',
    methods: {
        clickfun: function() {
            console.log('click')
        },
        mousein: function () {
            console.log('mouse in')
        },
        mouseout: function () {
            console.log('mouse out')
        },
        onSubmitFun: function (e) {

            console.log('submitted.')
        },
        onPressFun: function () {
            console.log('key pressed.')

        }

    },
});


var app5 = new Vue({

    el: '#app5',
    data: {
        name: '   whh  ',
        pwd:' abc d e',
        age: 12
    }
});

var app6 = new Vue({
    el: '#app6',
    data: {
        sex1: 'female',//性别
        sex2: ['female','male'],//取向可以多选
        article: `最佳答案: 睡眠是养生的第一大补,如果一个晚上不睡觉,十个白天也未必能换回来,长期如此,身体就会亏空,直接影响肝肾的健康运转,肾气虚弱就会直接导致头发的大量脱落。`,

        dist: 2,
        distMuti: [2,3,4]
    }

});

var app7 = new Vue({
    el: '#app7',
    data: {
        role: 'hr',
    }

});

var app8 = new Vue({
    el: '#app8',
    data: {

        chinese: 80,
        math: 70,
        english: 50,
    },
    methods: {
        sum1: function () {
            return parseFloat(this.chinese + this.math + this.english);
        }
    },
    computed: {
        sum: function () {
            return parseFloat(this.chinese + this.math + this.english);
        },

        average: function () {
            return Math.round(this.sum / 3);
        }


    }

});


// 组件
Vue.component('alert', {
    template: '<button @click="clickfun">点我啊</button>',
    methods: {
        clickfun: function() {

            alert('Yo.');
        }
    }
});

new Vue({
    el: '#seg1',


});

// haha_component可以抽取取出来，也可以写在域的components内部
var haha_component = {
    template: '<button @click="hahafun">点我就哈哈</button>',
    methods: {
        hahafun: function () {
            alert('Haha!');
        }
    }
};


new Vue({
    el: '#seg2',
    // haha 是标签名，即在html中，通过<haha></haha>使用组件
    // haha_component是定义的组件变量名
    components: {
        haha: haha_component
    }
});


// ===========点赞按钮===================
var like_component = {

    // 注意用法， :class="{类名: 条件}"  ，条件为真时，作用这个类
    template: `<button :class="{dianzan: liked}" @click="clickfun">
                    👍{{liked_count}}
                </button>`,
    methods: {
        clickfun: function () {
            if (!this.liked) {
                this.liked_count ++;
            } else {
                this.liked_count --;
            }
            this.liked = !this.liked;

        }
    },
    // 注意组件中的data要定义成函数，和定义域不同！
    data: function () {
        return {
            liked_count: 10,
            liked: false,
        }
    }

};

new Vue({
    el: '#app10',
    components: {
        like: like_component,
    }


});
// =============== 组件Component  父->子通信=======================
var say_yo_component = {
    template: '<button @click="sayYoFun">点我SayYo</button>',
    props: ['custom_msg'],
    methods: {
        sayYoFun: function () {
            alert(this.custom_msg);
        }
    }
};

var user_link_component = {

    //username是父标签user_link中的自定义参数
    // 注意单双引号混用时，需要转义
    template: '<a :href="\'/user/\' +username">@{{username}}</a>',
    props: ['username'],
};


new Vue({
    el: '#app11',
    components: {
        say_yo: say_yo_component,
        user_link: user_link_component,
    }
});

// =============子组件->父组件通信====================

// 注意注册全局组件是调用 Vue.componet('',{})函数，
// 我写成了Vue.component = ('',{}) 很气！！！
Vue.component('father',{
    template: `
    <div>
        <son @show-money="show_money_fun"> </son>
        <div v-if="flag">
        余额为98￥
        </div>
    </div>
    `,

    methods: {
        show_money_fun: function (data) {
            this.flag = true;
            alert("父收到了子的参数为 " + data);
            console.log(data);

        }
    },
    data: function () {
        return {
            flag: false,
        }
    },
});

Vue.component('son',{
    template:`<button @click="clickfun">显示余额</button>`,
    methods:{
        clickfun:function(){
            // this.$emit是由son标签发出的事件，所以也应由son标签监听该
            //  this.$emit还可以传参
            this.$emit('show-money',{a:1,b:1});
        }
    }
});

// ================任意两个组件通信====================================

// 通信的调度器
var Event = new Vue();

Vue.component('huahua',{
    // 注意template的内容必须包裹在一个根元素中，否则解析模板报错
    template: `
    <div>
    花花说了：<input v-model="huahua_said" @keyup="keyup_fun">{{huahua_said}}
    </div>`,
    // 组件中的属性，必须把data定义成function,并return属性和默认值组成的对象
    data:function () {
        return {
            huahua_said: '',
        }
    },
    methods: {
        keyup_fun: function () {
            //触发事件
            // Event.$emit第一个参数 是事件名称，可以自定义
            // 第二个参数是需要通信的数据
            Event.$emit('huahua_say_to_shuandan',this.huahua_said);
            console.log(this.huahua_said);
        }
    }

});

Vue.component('shuandan',{
    template:`
        <div>
            栓蛋听到花花的话，并重复了一遍：<input v-model="shuandan_listen" >
        </div>`,
    data: function () {
        return {
            shuandan_listen: ''
        }
    },
    // mounted表示挂载完毕（即组件刚初始化完成）这个时刻
    mounted: function () {
        // 在$on的外面，就缓存this的引用
        var me = this;

        //Event.$on第一个参数是需要监听的事件名
        // 第二个参数是监听的回调,默认就有data参数，data就是监听到的数据
        Event.$on('huahua_say_to_shuandan',function (data) {
            console.log('监听到了data=' + data);
            // 注意此时this已经变成了监听器，所以this.shuandan_listen = data;不生效
            console.log(this);
            // this.shuandan_listen = data;
            me.shuandan_listen = data;
        });
    }


});

new Vue({
    el: '#app13',

});

// =======================Filter===============================

// val 需要加单位的值
// unit 单位
Vue.filter('meter_filter', function (val,unit) {

    // 如果val为空，赋 默认值0
    val = val || 0;
    unit = unit || '米';
    return (val / 1000).toFixed(2) + unit;

});

new Vue({
    el: '#app14',
    data: {
        length: null, // 长度
    }

});

// =======================自定义指令基本用法==============================

// arg0,自定义指令名称，不加v-,仅在html标签使用时，为v-pin
// arg1,回调，两个参数分别是 el是v-pin所在的元素，binding是v-pin的值
Vue.directive('pin',function (el,binding) {

    var pinned = binding.value;
    console.log(pinned);

    if (pinned) {
        el.style.position = 'fixed';
        el.style.top = '10px';
        el.style.left = '10px';
    } else {
        el.style.position = 'static';
    }


});


new Vue({
    el: '#app15',
    data:{
        card1: {
            pinned:false,
        },
        card2: {
            pinned:false,
        },


    }
});

// ====================自定义指令 高级用法=================================
Vue.directive('pin2',function (el,binding) {

    var pinned = binding.value;
    console.log(pinned);


    var modifiers = binding.modifiers;
    console.log(modifiers);

    var arg = binding.arg;
    console.log('arg = ' + arg);

    if (pinned) {
        el.style.position = 'fixed';
        for (var key in modifiers) {
            if (modifiers[key]) {
                el.style[key] = '10px';
            }
        }
        if (arg) {
            el.style.background = 'yellow';
        }
    } else {
        el.style.position = 'static';
    }




});

new Vue({
    el: '#app16',
    data:{
        card1: {
            pinned:false,
        },
        card2: {
            pinned:false,
        },


    }
});
// =====================Vue mixins=========================
// popup和tooltip组件公共的地方是：data以及methods
// 因此抽取到mixins
// 另外mixins会被 data以及methods明确定义的东西，覆盖掉，无需担心！
var base = {
    methods: {
        toggle: function () {

            this.visible = !this.visible;

        },
        hide: function () {
            this.visible = false;
        },
        show: function () {
            this.visible = true;
        },



    },
    data: function() {
        return {
            visible: false,
        };
    },
}


// 定义一个弹出组件popup
Vue.component('popup', {
    template: `
        <div>
            <button @click="toggle">Popup(弹出)</button>
            <div v-if="visible" style="border:solid 1px">
                <!--比如用字母X模拟关闭-->
                <div @click="hide">X</div>
                <h4 >天生我材必有用 千金散尽还复来</h4>
            </div>
        </div>
    `,

    // 注意mixins接受一个数组
    mixins: [base],

});

// 再定义一个小提示组件 tooltip
Vue.component('tooltip',{
    template: `
    <div>
        <span @mouseenter="show" @mouseleave="hide"> 鼠标移入或者移出</span>
    
        <div v-if="visible">我是一条小提示!</div>
        
    </div>
    
    `,

    mixins: [base],

    // data中明确定义的东东会覆盖mixins！
    data: function () {
        return {
            visible: true
        }
    }




})

new Vue({
   el: '#app17', 
});

// =================slot 插槽==============================


Vue.component('panel',{

    // 引用html中的template标签id
    template: '#panel-tpl',

});


new Vue({
    el: '#app18',
});
